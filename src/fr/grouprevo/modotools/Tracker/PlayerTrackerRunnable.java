package fr.grouprevo.modotools.Tracker;


import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitTask;

import fr.grouprevo.modotools.RevoGames;
import fr.grouprevo.modotools.Utils.RevoPlayer;

public class PlayerTrackerRunnable implements Runnable{
	
	Player player;
	String playerName;
	RevoPlayer revoPlayer;
	BukkitTask task;
	Integer x = 0;
	
	public PlayerTrackerRunnable(Player p){
		this.player = p;
		this.playerName = p.getName();
		this.revoPlayer = RevoGames.get(player);
		this.task = Bukkit.getScheduler().runTaskTimer(RevoGames.getInstance(), this, 0, 1);
	}

	@Override
	public void run() {
		if(!player.isOnline()) cancel();
		
		if(revoPlayer.getCPS() >= 20){
			Bukkit.broadcastMessage("[RevoSafe] Il semblerait que " + playerName + " est un autoclick (" + revoPlayer.getCPS() + " cps)");
			System.out.println("[RevoSafe] Il semblerait que " + playerName + " est un autoclick (" + revoPlayer.getCPS() + " cps)");
			revoPlayer.addWarnings();
		}
		
		if(revoPlayer.getCPS() > revoPlayer.getMaxCPS()){
			revoPlayer.setMaxCPS(revoPlayer.getCPS());
		}
		
		if(x == 22){
			revoPlayer.clearCPS();
			x = 0;
		}else{
			x++;
		}
	}
	
	private void cancel(){
		task.cancel();
	}
}
