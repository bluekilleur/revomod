package fr.grouprevo.modotools.Commands;

import java.util.HashMap;
import java.util.Map.Entry;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;

import fr.grouprevo.modotools.RevoGames;
import fr.grouprevo.modotools.Utils.InventorySaver;

public class VerifCommand implements CommandExecutor {
	private final HashMap<Player,InventorySaver> inventorySaver = new HashMap<Player,InventorySaver>();
	public static boolean on = false;
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
		if(!(sender instanceof Player)){
			System.out.println("[RevoSafe] Vous ne pouvez pas utiliser cette commande depuis la console !");
		}else{
			final Player p = (Player) sender;
			if(p.isOp()){
				if(args.length == 0){
					p.sendMessage("/verif <pseudo>");
				}
				
				if(args.length > 1){
					p.sendMessage("/verif <pseudo>");
				}
				
				if(args.length == 1){
					final String verifName = args[0];
					final Player verif = Bukkit.getPlayer(verifName);
					if(verif != null){
						if(verif.isOnline()){
							final Inventory verifInv = Bukkit.createInventory(null, 45, "�cV�rif �4> �c" + verifName);
							p.openInventory(verifInv);
							on = true;
							new BukkitRunnable(){
								@Override
								public void run() {
									if(on){
										InventorySaver sis = inventorySaver.get(verif);
										sis = new InventorySaver();
										inventorySaver.put(verif,sis);
										sis.save(verif);
										
										if(sis.getHelmet() != null){
											ItemMeta helmetMeta = sis.getHelmet().getItemMeta();
											helmetMeta.setDisplayName("�6Casque �8: �f" + sis.getHelmet().getType().toString());
									        sis.getHelmet().setItemMeta(helmetMeta);
									        verifInv.setItem(0, sis.getHelmet());
										}
										
										if(sis.getChestplate() != null){
											ItemMeta chestplateMeta = sis.getChestplate().getItemMeta();
										    chestplateMeta.setDisplayName("�6Plastron �8: �f" + sis.getChestplate().getType().toString());
										    sis.getChestplate().setItemMeta(chestplateMeta);
										    verifInv.setItem(1, sis.getChestplate());
										}
										
										if(sis.getLeggings() != null){
											ItemMeta leggingsMeta = sis.getLeggings().getItemMeta();
									        leggingsMeta.setDisplayName("�6Pantalon �8: �f" + sis.getLeggings().getType().toString());
									        sis.getLeggings().setItemMeta(leggingsMeta);
									        verifInv.setItem(2, sis.getLeggings());
										}
										
										if(sis.getBoots() != null){
									        ItemMeta bootsMeta = sis.getBoots().getItemMeta();
									        bootsMeta.setDisplayName("�6Bottes �8: �f" + sis.getBoots().getType().toString());
									        sis.getBoots().setItemMeta(bootsMeta);
									        verifInv.setItem(3, sis.getBoots());
										}
								       
										ItemStack warn = new ItemStack(Material.REDSTONE_BLOCK);
								        ItemMeta warnMeta = warn.getItemMeta();
								        warnMeta.setDisplayName("�6Nombre de warnings �8: �f" + RevoGames.get(verif).getWarnings());
								        warn.setItemMeta(warnMeta);
								        warn.setAmount(RevoGames.get(verif).getWarnings());
								        
								        ItemStack ping = new ItemStack(Material.IRON_BLOCK);
								        ItemMeta pingMeta = ping.getItemMeta();
								        pingMeta.setDisplayName("�6Ping du joueur �8: �f" + RevoGames.get(verif).getPing());
								        ping.setItemMeta(pingMeta);
								        ping.setAmount(RevoGames.get(verif).getPing());
								        
								        ItemStack cpss = new ItemStack(Material.GOLD_BLOCK);
								        ItemMeta cpssMeta = cpss.getItemMeta();
								        cpssMeta.setDisplayName("�6Nombre de clics actuellement �8: �f" + RevoGames.get(verif).getCPS());
								        cpss.setItemMeta(cpssMeta);
								        cpss.setAmount(RevoGames.get(verif).getCPS());
								        
								        ItemStack cpsm = new ItemStack(Material.DIAMOND_BLOCK);
								        ItemMeta cpsmMeta = cpsm.getItemMeta();
								        cpsmMeta.setDisplayName("�6Nombre Maximum de clics �8: �f" + RevoGames.get(verif).getMaxCPS());
								        cpsm.setItemMeta(cpsmMeta);
								        cpsm.setAmount(RevoGames.get(verif).getMaxCPS());
								        
								        for(final Entry<Integer, ItemStack> entry:sis.getContents().entrySet()){
								        	verifInv.setItem(entry.getKey() + 9, entry.getValue());
								        }
					                        
								        verifInv.setItem(5, warn);
								        verifInv.setItem(6, ping);
								        verifInv.setItem(7, cpss);
								        verifInv.setItem(8, cpsm);
								        p.updateInventory();
								        sis.clear();
								        verifInv.clear();
									}else{
										cancel();
									}
								}
							}.runTaskTimer(RevoGames.getInstance(), 0, 1);
							
						}else{
							p.sendMessage("[RevoSafe] Ce joueur n'est pas connect� !");
						}
					}else{
						p.sendMessage("[RevoSafe] Ce joueur n'existe pas !");
					}
				}
			}else{
				p.sendMessage("[RevoSafe] Vous n'avez pas la permission d'�ffectuer cette commande !");
			}
		}
		return false;
	}
}
