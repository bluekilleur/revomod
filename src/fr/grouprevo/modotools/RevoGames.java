package fr.grouprevo.modotools;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitTask;

import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.ProtocolManager;

import fr.grouprevo.modotools.Managers.CommandsManager;
import fr.grouprevo.modotools.Managers.ListenersManager;
import fr.grouprevo.modotools.Managers.ManagerProtocol;
import fr.grouprevo.modotools.Tracker.PlayerTrackerRunnable;
import fr.grouprevo.modotools.Utils.RevoPlayer;


public class RevoGames extends JavaPlugin implements Listener{
	
	private static RevoGames instance;
	
	private static Map<String, RevoPlayer> revoPlayer = new HashMap<>();
	private static List<BukkitTask> runningTasks = new ArrayList<>();
	private static ProtocolManager protocolManager;
	
	@Override
	public void onLoad() {
		protocolManager = ProtocolLibrary.getProtocolManager();
	}
	
	@Override
    public void onEnable() {
		instance = this;
		
		new ListenersManager().setup();
		new CommandsManager().setup();
		
		ManagerProtocol.setupProtocols();
		reload();
	}
	
	@Override
	public void onDisable(){
		
	}
	
	public static RevoGames getInstance(){
		return instance;
	}
	
	public static ProtocolManager getProtocolManager(){
		return protocolManager;
	}
	
	@SuppressWarnings("deprecation")
	public static void reload(){
		for (BukkitTask task : runningTasks){
			task.cancel();
		}
		for(Player player : Bukkit.getOnlinePlayers()){
			initPlayer(player);
		}
	}
	
	public static RevoPlayer get(Player player){
		return revoPlayer.get(player.getName());
	}

	public static void initPlayer(Player player){
		if(get(player) == null) revoPlayer.put(player.getName(), new RevoPlayer(player));
		new PlayerTrackerRunnable(player);
	}
}
