package fr.grouprevo.modotools.Protocols;

import org.bukkit.plugin.Plugin;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.events.ListenerPriority;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketEvent;

import fr.grouprevo.modotools.RevoGames;

public class playerInteract {
	
	private static boolean on = true;
	
	public static void setupListener(){
		if(!on)return;
		Plugin plugin = RevoGames.getInstance();
		
		RevoGames.getProtocolManager().addPacketListener(new PacketAdapter(plugin, ListenerPriority.NORMAL, PacketType.Play.Client.ARM_ANIMATION) {
			@Override
			public void onPacketReceiving(PacketEvent e){
				if(e.getPacketType() == PacketType.Play.Client.ARM_ANIMATION){
					try {
						RevoGames.get(e.getPlayer()).addCPS();
					} catch (Exception ex){
						ex.printStackTrace();
					}
					
				}
			}
		});
	}
}
