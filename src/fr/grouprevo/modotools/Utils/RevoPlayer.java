package fr.grouprevo.modotools.Utils;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class RevoPlayer {

	public String playerName;
	public Player player;
	public Integer warnings = 0;
	public Integer cps = 0;
	public Integer maxcps = 0;
	public Boolean verif = false;
	
	Class<?> CPClass;
	String serverName  = Bukkit.getServer().getClass().getPackage().getName();
	String serverVersion = serverName.substring(serverName.lastIndexOf(".") + 1, serverName.length());
	
	
	public RevoPlayer(Player p){
		this.playerName = p.getName();
		this.player = p;
	}
	
	public Integer getWarnings(){
		return this.warnings;
	}
	
	public void setWarnings(Integer warnings){
		this.warnings = warnings;
	}
	
	public void addWarnings(){
		this.warnings++;
	}
	
	public void removeWarnings(){
		this.warnings--;
	}
	
	public Player getPlayer(){
		return this.player;
	}
	
	public String getPlayerName(){
		return this.playerName;
	}
	
	public Integer getCPS(){
		return this.cps;
	}
	
	public Integer getMaxCPS(){
		return this.maxcps;
	}
	
	public void setMaxCPS(Integer cps){
		this.maxcps = cps;
	}
	
	public void addCPS(){
		this.cps++;
	}
	
	public void clearCPS(){
		this.cps = 0;
	}
	
	public void clearMaxCPS(){
		this.maxcps = 0;
	}
	

	public int getPing() {
		try {
			CPClass = Class.forName("org.bukkit.craftbukkit." + serverVersion + ".entity.CraftPlayer");
			Object CraftPlayer = CPClass.cast(this.player);

			Method getHandle = CraftPlayer.getClass().getMethod("getHandle", new Class[0]);
			Object EntityPlayer = getHandle.invoke(CraftPlayer, new Object[0]);

			Field ping = EntityPlayer.getClass().getDeclaredField("ping");

			return ping.getInt(EntityPlayer);
		} catch (Exception e) {
				e.printStackTrace();
		}
		return 0;
	}
	
}
