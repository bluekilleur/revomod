package fr.grouprevo.modotools.Utils;

import java.util.HashMap;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
 
public class InventorySaver {
 
        private final HashMap<Integer,ItemStack> contents = new HashMap<Integer,ItemStack>();
        
        private ItemStack helmet = null;
        private ItemStack chestplate = null;
        private ItemStack leggings = null;
        private ItemStack boots = null;
 
        public final void clear(){
                contents.clear();
                helmet = null;
                chestplate = null;
                leggings = null;
                boots = null;
        }
 
        public final void save(final Player player){
                clear();
                helmet = player.getInventory().getHelmet();
               // System.out.println("helmet >" + helmet);
                
                chestplate = player.getInventory().getChestplate();
               // System.out.println("chestplate >" + chestplate);
                
                leggings = player.getInventory().getLeggings();
               // System.out.println("leggings >" + leggings);
                
                boots = player.getInventory().getBoots();
               // System.out.println("boots >" + boots);
 
                for(int i = 0;i<player.getInventory().getContents().length;i++){
                        final ItemStack itemStack = player.getInventory().getContents()[i];
                        if(itemStack!=null){
                        	//System.out.println("item["+i+"] >" + itemStack);
                        	contents.put(i,itemStack.clone());
                        }
                }
        }
        
        public final ItemStack getHelmet(){
        	return helmet;
        }
        
        public final ItemStack getChestplate(){
        	return chestplate;
        }
        
        public final ItemStack getLeggings(){
        	return leggings;
        }
        
        public final ItemStack getBoots(){
        	return boots;
        }
        
        public final HashMap<Integer,ItemStack> getContents(){
        	return contents;
        }
 
}