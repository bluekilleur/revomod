package fr.grouprevo.modotools.Managers;

import org.bukkit.command.CommandExecutor;

import fr.grouprevo.modotools.RevoGames;
import fr.grouprevo.modotools.Commands.VerifCommand;

public class CommandsManager {
	
	public CommandsManager(){}
	
	public void setup(){
		CommandExecutor verif = new VerifCommand();
		RevoGames.getInstance().getCommand("verif").setExecutor(verif);
	}
}
