package fr.grouprevo.modotools.Managers;

import org.bukkit.plugin.PluginManager;

import fr.grouprevo.modotools.RevoGames;
import fr.grouprevo.modotools.Listeners.playerCloseInventoryEvent;
import fr.grouprevo.modotools.Listeners.playerJoinEvent;
import fr.grouprevo.modotools.Listeners.playerQuitEvent;

public class ListenersManager {
	
	public ListenersManager(){}
	
	public void setup(){
		PluginManager pm = RevoGames.getInstance().getServer().getPluginManager();
		pm.registerEvents(new playerCloseInventoryEvent(), RevoGames.getInstance());
		pm.registerEvents(new playerJoinEvent(), RevoGames.getInstance());
		pm.registerEvents(new playerQuitEvent(), RevoGames.getInstance());
	}
}
