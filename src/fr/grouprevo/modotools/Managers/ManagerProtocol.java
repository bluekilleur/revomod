package fr.grouprevo.modotools.Managers;

import com.comphenix.protocol.ProtocolManager;

import fr.grouprevo.modotools.RevoGames;
import fr.grouprevo.modotools.Protocols.playerInteract;

public class ManagerProtocol {
	
	@SuppressWarnings("unused")
	private static ProtocolManager protocolManager;
	
	public static void setupProtocols(){
		protocolManager = RevoGames.getProtocolManager();
		playerInteract.setupListener();
	}
}
